datatype tree = nullTree
		| node of tree * int * tree;

fun inorder nullTree = []
	| inorder (node (nullTree, i, nullTree)) = [i]
	| inorder (node (ltree, i, rtree)) = inorder(ltree)@[i]@inorder(rtree);

val testTree = node (node (node (nullTree, 1, nullTree), 2, node (nullTree, 3, nullTree)), 4, node (node (nullTree, 5, nullTree), 6, node (nullTree, 7, nullTree)))


fun rotate nullTree = nullTree
	| rotate (node (ltree, i, rtree)) = node ((), rtree[0], ());


inorder(testTree);
rotate(testTree);
