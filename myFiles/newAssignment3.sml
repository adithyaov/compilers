datatype GrmItem = T of string | NT of string;
type GrmRule = GrmItem * (GrmItem list);
type GrmRules = GrmRule list;

fun compareNT (NT x, NT y) = String.compare(x, y)
  | compareNT (T x, T y) = String.compare(x, y)
  | compareNT _ = LESS;

structure Map = RedBlackMapFn (struct
    type ord_key = GrmItem
    val compare = compareNT
end);

fun compareMap map1 map2 = Map.listItems (map1) = Map.listItems (map2);

fun getProper ifNone NONE = ifNone
  | getProper ifNone rest = valOf(rest);

fun isNullable nullMap y [] = Map.insert (nullMap, y, true)
  | isNullable nullMap y [(T " ")] = Map.insert (nullMap, y, true)
  | isNullable nullMap y (x::xs) = if getProper false (Map.find (nullMap, x)) then isNullable nullMap y xs else nullMap;

fun nullRules nullMap [] = nullMap
  | nullRules nullMap ((x, y::ys)::rules) = nullRules (isNullable nullMap x (y::ys)) rules;

fun loopTillConvergenceNull rules nullMap prevMap = if compareMap prevMap (nullRules nullMap rules) then prevMap
                                                        else loopTillConvergenceNull rules (nullRules nullMap rules) nullMap;

fun isolate [] = []
  | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs);
fun union l1 l2 = isolate (l1@l2);

fun findFirst nullMap firstMap x [] = firstMap
  | findFirst nullMap firstMap x ((T t)::_) =
    let
      val fx = getProper [] (Map.find(firstMap, x))
    in
      (Map.insert(firstMap, x, union fx [(T t)]))
    end
  | findFirst nullMap firstMap x (y::ys) =
    let
      val fx = getProper [] (Map.find(firstMap, x))
      val fy = getProper [] (Map.find(firstMap, y))
    in
      if getProper false (Map.find(nullMap, y)) then findFirst nullMap (Map.insert(firstMap, x, union fx fy)) x ys
      else Map.insert(firstMap, x, union fx fy)
    end;

fun firstRules nullMap firstMap [] = firstMap
  | firstRules nullMap firstMap ((x, y::ys)::rules) = firstRules nullMap (findFirst nullMap firstMap x (y::ys)) rules;

fun loopTillConvergenceFirst rules nullMap firstMap prevMap = if compareMap prevMap (firstRules nullMap firstMap rules) then prevMap
                                                        else loopTillConvergenceFirst rules nullMap (firstRules nullMap firstMap rules) firstMap;

fun findFirstList rl nullMap firstMap [] = rl
  | findFirstList rl nullMap firstMap ((T t)::_) = (union rl [(T t)])
  | findFirstList rl nullMap firstMap (x::xs) =
    if getProper false (Map.find(nullMap, x)) then
      findFirstList (union rl (getProper [] (Map.find(firstMap, x)))) nullMap firstMap xs
    else (union rl (getProper [] (Map.find(firstMap, x))));

fun checkCompleteNullable nullMap [] = true
  | checkCompleteNullable nullMap (y::items) = if (getProper false (Map.find(nullMap, y))) then (checkCompleteNullable nullMap items) else false;


fun findFollow nullMap firstMap followMap a [] = followMap
  | findFollow nullMap firstMap followMap a ((T t)::bs) = findFollow nullMap firstMap followMap a bs
  | findFollow nullMap firstMap followMap a (b::bs) =
    let
      val flist = findFirstList [] nullMap firstMap bs
      val bf = getProper [] (Map.find(followMap, b))
      val fa = getProper [] (Map.find(followMap, a))
    in
      if checkCompleteNullable nullMap bs then (findFollow nullMap firstMap (Map.insert(followMap, b, union (union bf flist) fa)) a bs)
      else (findFollow nullMap firstMap (Map.insert(followMap, b, union bf flist)) a bs)
    end;

fun followRules nullMap firstMap followMap [] = followMap
  | followRules nullMap firstMap followMap ((x, y::ys)::rules) = followRules nullMap firstMap (findFollow nullMap firstMap followMap x (y::ys)) rules;

fun loopTillConvergenceFollow rules nullMap firstMap followMap prevMap = if compareMap prevMap (followRules nullMap firstMap followMap rules) then prevMap
                                                else loopTillConvergenceFollow rules nullMap firstMap (followRules nullMap firstMap followMap rules) followMap;

fun chainOrder EQUAL LESS = LESS
  | chainOrder EQUAL GREATER = GREATER
  | chainOrder LESS _ = LESS
  | chainOrder GREATER _ = GREATER
  | chainOrder _ _ = LESS;
fun compareLL ((NT x, T y), (NT a, T b)) = if [x, a] = [y, b] then EQUAL else (chainOrder (String.compare(x, a)) (String.compare(y, b)))
  | compareLL _ = LESS;

structure MapLL = RedBlackMapFn (struct
    type ord_key = GrmItem * GrmItem
    val compare = compareLL
end);

fun insertInTable x rule [] table = table
  | insertInTable x rule ((T " ")::ys) table = insertInTable x rule ys table
  | insertInTable x rule (y::ys) table = insertInTable x rule ys (MapLL.insert(table, (x, y), union (getProper [] (MapLL.find(table, (x, y)))) [rule]));


fun makeLL1Table nullMap firstMap followMap [] table = table
  | makeLL1Table nullMap firstMap followMap (((NT x), production)::rules) table =
      let
        val firstRHS = findFirstList [] nullMap firstMap production
        val followLHS = getProper [] (Map.find(followMap, NT x))
      in
        if checkCompleteNullable nullMap production then makeLL1Table nullMap firstMap followMap rules (insertInTable (NT x) ((NT x), production) (union firstRHS followLHS) table)
        else makeLL1Table nullMap firstMap followMap rules (insertInTable (NT x) ((NT x), production) firstRHS table)
      end;


(*val rules = [((NT "Z"), [(T "d")]), ((NT "Z"), [(NT "X"), (NT "Y"), (NT "Z"), (T "$")]), ((NT "X"), [(NT "Y")]), ((NT "Y"), [(T " ")]), ((NT "Y"), [(T "c")]), ((NT "X"), [(T "a")])];*)
val rules = [(NT "E", [NT "E", T "+", NT "E"]), (NT "E", [NT "E", T "*", NT "E"]), (NT "E", [T "x"])];


val NM = loopTillConvergenceNull rules Map.empty Map.empty;
val FM = loopTillConvergenceFirst rules NM Map.empty Map.empty;
val FLM = loopTillConvergenceFollow rules NM FM Map.empty Map.empty;
Map.listItemsi(NM);
Map.listItemsi(FM);
Map.listItemsi(FLM);
val parsingTable = makeLL1Table NM FM FLM rules (MapLL.empty);
MapLL.listItemsi(parsingTable);

(* 0 lookahead *)

type pos = int
type len = int

fun lookUpPos (x::_) 0 = x
  | lookUpPos (x::xs) i = lookUpPos xs (i-1)
  | lookUpPos [] _ = T "_";

fun getRulesOf [] x result = result
  | getRulesOf ((x', (y::ys))::rules) x result =
      if x = x' then getRulesOf rules x (union result [((x', (y::ys)), 0, List.length(y::ys))]) else getRulesOf rules x result;

fun findClosure rules x (y::ys) pos len = if pos < len then getRulesOf rules (lookUpPos (y::ys) pos) [] else [];

fun closureStep rules [] result = result
  | closureStep rules (((x, y::ys), pos, len)::items) result = closureStep rules items (union result (findClosure rules x (y::ys) pos len));


fun untilConvergenceClosure rules currentItems prevItems = if (currentItems = prevItems) then prevItems
                                  else untilConvergenceClosure rules (union currentItems (closureStep rules currentItems [])) currentItems;

fun goTo rules [] x' j = untilConvergenceClosure rules j []
  | goTo rules (((x, y::ys), pos, len)::items) x' j =
      if pos < len then
        if (lookUpPos (y::ys) pos) = x' then goTo rules items x' (union j [((x, y::ys), pos + 1, len)])
        else goTo rules items x' j
      else goTo rules items x' j;

(*

val rules = [((NT "S"), [(NT "X")]), ((NT "X"), [(NT "Y"), (NT "Z")]), ((NT "Y"), [(T "a")]), ((NT "Y"), [(T "b")]), ((NT "Z"), [(T "a")])];*)

goTo rules [(((NT "X"), [(NT "Y"), (NT "Z")]), 0, 2)] (NT "Y") [];

fun simulateGoTo t e [] rules = (t, e)
  | simulateGoTo t e (((x, y::ys), pos, len)::items) rules =
      let val j = goTo rules (((x, y::ys), pos, len)::items) (lookUpPos (y::ys) pos) []
      in simulateGoTo (union t [j]) (union e [((((x, y::ys), pos, len)::items), (lookUpPos (y::ys) pos), j)]) items rules end;

fun simulateDFA (state::states) e rules =
      let val (nt, ne) = simulateGoTo (state::states) e state rules
      in
        if nt = (state::states) then
          if ne = e then (nt, e) else simulateDFA nt ne rules
        else simulateDFA nt ne rules
      end;

fun gRSLR1LoopToken [] state rule reduceList = reduceList
  | gRSLR1LoopToken (t::tokens) state rule reduceList = gRSLR1LoopToken tokens state rule (union reduceList [(state, t, rule)]);

fun gSLR1LoopItem followMap [] state reduceList = reduceList
  | gSLR1LoopItem followMap (((x, y::ys), pos, len)::rules) state reduceList =
      gSLR1LoopItem followMap rules state (union reduceList (gRSLR1LoopToken (getProper [] (Map.find(followMap, x))) state (x, y::ys) reduceList));

fun gSLR1LoopState followMap [] reduceList = reduceList
  | gSLR1LoopState followMap (state::states) reduceList =
      gSLR1LoopState followMap states (union reduceList (gSLR1LoopItem followMap state state reduceList));

fun addEdgePos [] table = table
  | addEdgePos ((i, (T x), j)::edges) table = addEdgePos edges (union table [(i, (T x), ("shift", j))])
  | addEdgePos ((i, (NT x), j)::edges) table = addEdgePos edges (union table [(i, (NT x), ("goto", j))]);

fun addAcceptAction state [] table = table
  | addAcceptAction state (((x, y::ys), pos, len)::items) table =
      if lookUpPos (y::ys) pos = (T "$") then addAcceptAction state items (union table [(state, (T "$"), ("accept", []))])
      else addAcceptAction state items table;

fun addAcceptActionStates [] table = table
  | addAcceptActionStates (state::states) table = addAcceptActionStates states (union table (addAcceptAction state state table));

fun addReductions [] table = table
  | addReductions ((i, z, rule)::reduceList) table = addReductions reduceList (union table [(i, z, ("reduce", []))]);

fun makeSLRTable shiftsAndGotos reduces = (shiftsAndGotos, reduces);

val NM = loopTillConvergenceNull rules Map.empty Map.empty;
val FM = loopTillConvergenceFirst rules NM Map.empty Map.empty;
val FLM = loopTillConvergenceFollow rules NM FM Map.empty Map.empty;
val (t, e) = simulateDFA [(untilConvergenceClosure rules [(((NT "S'"), [(NT "S"), (T "$")]), 0, 1)] [])] [] rules;
val r = gSLR1LoopState FLM t [];

(* SLR => t + e + r *)

val shiftsAndGotos = (addEdgePos e [])@(addAcceptActionStates t []);
val reduces = (addReductions r []);

makeSLRTable shiftsAndGotos reduces;


OS.Process.exit(OS.Process.success);
