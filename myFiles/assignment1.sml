signature ORD =	sig
	type t
	val le : t -> t -> bool
end;

structure InOrd:ORD = struct
	type t = int
	val le = fn m => fn n => (m >= n)
end;


functor QS (X:ORD) =
	struct
		fun qSort [] = []
			| qSort (head::tail) = 
				let
					val partitions = List.partition (X.le head) tail
				in
					(qSort (#1 partitions))@[head]@(qSort (#2 partitions))
				end 
	end;

structure TrueQSort = QS (InOrd);

TrueQSort.qSort [3,2,1,4,5,6];
