structure Cfg =
struct

datatype Terminal = Eps | T of string;
datatype NonTerminal = NT of string;
datatype GrmType = GT of Terminal | GNT of NonTerminal;
datatype GrmRule = Rule of NonTerminal * (GrmType list);
type GrmRules = GrmRule list;

fun lookUpProductions (productionsOf, (nonT, prod)::ruleListTail, returnProductions) =
    if productionsOf = nonT then
        lookUpProductions (productionsOf, ruleListTail, (nonT, prod)::returnProductions)
    else lookUpProductions (productionsOf, (nonT, prod)::ruleListTail, returnProductions)
  | lookUpProductions (_, [], returnProductions) = returnProductions;

fun isNullable (NT(x)) = 
    let productionsOfX = lookUpProductions(NT(x), ruleList, [])
    in 
        
    end
    


(*

fun findFirst ((nonT, (prodHd: Terminal)::prodTl)::ruleListTail, firstList) =
                findFirst (ruleListTail, (nonT, prodHd)::firstList)
  | findFirst ((nonT, (prodHd: NonTerminal)::prodTl)::ruleListTail, firstList) =

*)



end;
