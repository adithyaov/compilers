(*Helper functions*)
fun len [] = 0
    | len (_::items) = 1 + (len items);

fun isolate [] = []
    | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs)

(*Signature requires for a graph*)
signature NODESIG = sig
    eqtype node
    val compareNodes: (node * node) -> order
end;

(*Basic functor for making the graph with the above signature*)
functor GraphFn (G:NODESIG) = struct
  structure MapGraph = RedBlackMapFn (struct
      type ord_key = G.node
      val compare = G.compareNodes
  end);

  fun getProper ifNone NONE = ifNone
      | getProper ifNone rest = valOf(rest);

  fun containedIn n1 [] = false
      | containedIn n1 (n::ns) = if (G.compareNodes (n1, n) = EQUAL) then true else (containedIn n1 ns);

  fun newNode mapG n = MapGraph.insert(mapG, n, [] : G.node list);
  fun addEdge mapG n ns = MapGraph.insert(mapG, n, (getProper [] (MapGraph.find(mapG, n)))@ns);
  fun nodes mapG = MapGraph.listKeys(mapG);
  fun succ mapG ofN = getProper [] (MapGraph.find(mapG, ofN));
  fun pred [] _ _ = []
      | pred (n::ns) mapG ofN = if containedIn ofN (getProper [] (MapGraph.find(mapG, n))) then n::(pred ns mapG ofN) else (pred ns mapG ofN);
  fun isIn [] n = false
      | isIn (n1::nodes) n = if G.compareNodes (n1, n) = EQUAL then true else isIn nodes n;


  (*Function for traversing down*)
  fun traverseDown G n pN bNode =
      let
          val pred = (pred (nodes G) G n)
          val succ = (succ G n)
      in
          if isIn [n] bNode then pN else
          if len pred > 1 then pN else
          if len succ > 1 then n else
          if len succ = 0 then n else
              traverseDown G (hd succ) n bNode
      end;

  (*Function for traversing up*)
  fun traverseUp G n pN bNode =
      let
          val pred = (pred (nodes G) G n)
          val succ = (succ G n)
      in
          if isIn [n] bNode then n else
          if len succ > 1 then pN else
          if len pred > 1 then n else
          if len pred = 0 then n else
              traverseUp G (hd pred) n bNode
      end;

  (*Function for finding the basic blocks*)
  fun findBB [] G bb = isolate bb
      | findBB (n::ns) G bb =
          let
              val pred = (pred (nodes G) G n)
              val succ = (succ G n)
          in
              if len pred = 1 andalso len succ = 1 then
                findBB ns G ((traverseUp G (hd pred) n n, traverseDown G (hd succ) n n)::bb)
              else if len succ = 1 then
                findBB ns G ((n, traverseDown G (hd succ) n n)::bb)
              else if len pred = 1 then
                findBB ns G ((traverseUp G (hd pred) n n, n)::bb)
              else findBB ns G ((n, n)::bb)
          end;

  fun isInBB [] (a1, b1) = false
      | isInBB ((a, b)::zs) (a1, b1) = if a1 = a then true else isInBB zs (a1, b1);

  fun isolateBB [] newList = newList
      | isolateBB ((a, b)::xs) newList = if isInBB newList (a, b) = false then isolateBB xs ((a, b)::newList) else isolateBB xs newList;

  fun rmDup [] _ = []
      | rmDup [x] _ = [x]
      | rmDup ((a, b)::bbList) l = if l = a then bbList else (a, b)::(rmDup bbList l);

  (* filter for detecting cycles *)
  fun newBBCyc G [] bbList = bbList
      | newBBCyc G (n::ns) bbList =
          let
              val predBe = (pred (nodes G) G n)
              val succBe = (succ G n)
          in
              if (len predBe = 1) andalso (len succBe = 1) then
                  let
                      val (pU::_) = predBe
                      val (sU::_) = succBe
                      val p = (pred (nodes G) G pU)
                      val s = (succ G sU)
                  in
                      if (len p = 1) andalso (len s = 1) then
                          newBBCyc G ns (rmDup bbList n)
                      else newBBCyc G ns bbList
                  end
              else newBBCyc G ns bbList
          end;

(* --------------------------------------------------------------------------- *)

  (* Initialize *)
  fun initInMap inMap useMap [] = inMap
      | initInMap inMap useMap (node::nodes) =
          let
            val valueToInsert = getProper (AtomSet.empty) (MapGraph.find(useMap, node))
            val newInMap = MapGraph.insert(inMap, node, valueToInsert)
          in
            initInMap newInMap useMap nodes
          end;

  fun unionAtomSets theMap [] = AtomSet.empty
      | unionAtomSets theMap (node::nodes) =
          AtomSet.union(getProper (AtomSet.empty) (MapGraph.find(theMap, node)), unionAtomSets theMap nodes);

  fun findSetStep G inMap outMap useMap defMap [] = (inMap, outMap)
      | findSetStep G inMap outMap useMap defMap (node::nodes) =
          let
            val useSet = getProper (AtomSet.empty) (MapGraph.find(useMap, node))
            val defSet = getProper (AtomSet.empty) (MapGraph.find(defMap, node))
            val newOutSet = unionAtomSets inMap (succ G node)
            val newInSet = AtomSet.union(useSet, AtomSet.difference(newOutSet, defSet))
            val newInMap = MapGraph.insert(inMap, node, newInSet)
            val newOutMap = MapGraph.insert(outMap, node, newOutSet)
          in
            findSetStep G newInMap newOutMap useMap defMap nodes
          end;

  fun compareAtomMaps map1 map2 [] = true
      | compareAtomMaps map1 map2 (node::nodes) =
          let
            val map1Set = getProper (AtomSet.empty) (MapGraph.find(map1, node))
            val map2Set = getProper (AtomSet.empty) (MapGraph.find(map2, node))
          in
            if AtomSet.equal(map1Set, map2Set) then compareAtomMaps map1 map2 nodes else false
          end;

  fun killGenSet useMap defMap genSet killSet [] = (genSet, killSet)
      | killGenSet useMap defMap genSet killSet (node::nodes) =
          let
            val useSet = getProper (AtomSet.empty) (MapGraph.find(useMap, node))
            val defSet = getProper (AtomSet.empty) (MapGraph.find(defMap, node))
            val newGenSet = AtomSet.union(genSet, AtomSet.difference(useSet, killSet))
            val newKillSet = AtomSet.union(killSet, defSet)
          in
            killGenSet useMap defMap newGenSet newKillSet nodes
          end;

  fun getInstList G startInst result =
        let
          val succBe = succ G startInst;
        in
          if len succBe = 1 then
            if isIn result startInst then
              result
            else getInstList G (hd succBe) (startInst::result)
          else (startInst::result)
        end;

  fun makeKillGenSetOfBasicBlocks G useMap defMap result [] = result
      | makeKillGenSetOfBasicBlocks G useMap defMap result ((startInst, endInst)::nodes) =
          let
            val instList = getInstList G startInst []
            val (genSet, killSet) = killGenSet useMap defMap (AtomSet.empty) (AtomSet.empty) instList
          in
            makeKillGenSetOfBasicBlocks G useMap defMap (((startInst, endInst), (genSet, killSet))::result) nodes
          end;

  fun fixedPtCalcInMapOutMap G inMap outMap useMap defMap =
        let
          val (newInMap, newOutMap) = findSetStep G inMap outMap useMap defMap (nodes G)
          val compareInMaps = compareAtomMaps newInMap inMap (nodes G)
          val compareOutMaps = compareAtomMaps newOutMap outMap (nodes G)
        in
          if (compareInMaps andalso compareOutMaps) then (inMap, outMap)
          else fixedPtCalcInMapOutMap G newInMap newOutMap useMap defMap
        end;

end;

(*A string node structure*)
structure StringNode = struct
        datatype node = N of string
        val compareNodes = fn ((N a), (N b)) => String.compare(a, b)
    end;

(*A String graph*)
structure Graph = GraphFn(StringNode);


(*Creating the Global graph structure*)
structure GlobalNode = struct
        datatype node = GN of (StringNode.node * StringNode.node)
        val compareNodes = fn (GN (a1, b1), GN (a2, b2)) =>
            if StringNode.compareNodes(a1, a2) = EQUAL then StringNode.compareNodes(b1, b2) else StringNode.compareNodes(a1, a2)
    end;

(*GlobalGraph == Graph of basic blocks as nodes*)
structure GlobalGraph = GraphFn(GlobalNode);

(*Functions for making the GlobalGraph*)

fun findFlow nodes [] = []
    | findFlow nodes ((n1, n2)::ns) = if Graph.isIn nodes n1 then ((GlobalNode.GN (n1, n2))::(findFlow nodes ns)) else findFlow nodes ns;

fun findSucc G (n1, n2) bbList = findFlow (Graph.succ G n2) bbList;


fun makeGraph G [] newG bbFull = newG
    | makeGraph G (bb::bbList) newG bbFull =
          makeGraph G bbList (GlobalGraph.addEdge (GlobalGraph.newNode newG (GlobalNode.GN bb)) (GlobalNode.GN bb) (findSucc G bb bbFull)) bbFull;

(* --------------------------------------------------------------------------------- *)

fun getProper ifNone NONE = ifNone
    | getProper ifNone rest = valOf(rest);


fun makeAtomSetOutOfSetOfStrings set [] = set
    | makeAtomSetOutOfSetOfStrings set (x::xs) = makeAtomSetOutOfSetOfStrings (AtomSet.add(set, Atom.atom x)) xs;

fun makeStringsOutOfSetOfAtomSet strList [] = strList
    | makeStringsOutOfSetOfAtomSet strList (x::xs) = makeStringsOutOfSetOfAtomSet ((Atom.toString x)::strList) xs;

fun populateGenMapAndKillMap genMap killMap [] = (genMap, killMap)
    | populateGenMapAndKillMap genMap killMap ((entry, (genSet, killSet))::nodes) =
        let
          val mainGenMap = GlobalGraph.MapGraph.insert(genMap, entry, genSet)
          val manKillMap = GlobalGraph.MapGraph.insert(killMap, entry, killSet)
        in
          populateGenMapAndKillMap mainGenMap manKillMap nodes
        end;

fun deConstructGlobal [] = []
    | deConstructGlobal ((GlobalNode.GN(a, b))::nodes) = (a, b)::(deConstructGlobal nodes)

fun makeDualMap genMap killMap [] = (genMap, killMap)
    | makeDualMap genMap killMap ((node, (genSet, killSet))::nodes) =
        let
          val newGenMap = GlobalGraph.MapGraph.insert(genMap, GlobalNode.GN node, genSet)
          val newKillMap = GlobalGraph.MapGraph.insert(killMap, GlobalNode.GN node, killSet)
        in
          makeDualMap newGenMap newKillMap nodes
        end;


(* ------------------------------------------------------------------------------ *)

val g1 = Graph.MapGraph.empty;
val g1 = Graph.newNode g1 (StringNode.N "I1");
val g1 = Graph.newNode g1 (StringNode.N "I2");
val g1 = Graph.newNode g1 (StringNode.N "I3");
val g1 = Graph.newNode g1 (StringNode.N "I4");
val g1 = Graph.addEdge g1 (StringNode.N "I1") [(StringNode.N "I2")];
val g1 = Graph.addEdge g1 (StringNode.N "I2") [(StringNode.N "I3")];
val g1 = Graph.addEdge g1 (StringNode.N "I2") [(StringNode.N "I4")];
val useMap = Graph.MapGraph.empty;
val defMap = Graph.MapGraph.empty;
val useMap = Graph.MapGraph.insert(useMap, StringNode.N "I1", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["y", "z"]);
val defMap = Graph.MapGraph.insert(defMap, StringNode.N "I1", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x"]);
val useMap = Graph.MapGraph.insert(useMap, StringNode.N "I2", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w", "z"]);
val defMap = Graph.MapGraph.insert(defMap, StringNode.N "I2", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x"]);
val useMap = Graph.MapGraph.insert(useMap, StringNode.N "I3", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w", "z"]);
val defMap = Graph.MapGraph.insert(defMap, StringNode.N "I3", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w"]);
val useMap = Graph.MapGraph.insert(useMap, StringNode.N "I4", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x", "y"]);
val defMap = Graph.MapGraph.insert(defMap, StringNode.N "I4", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["z"]);


(* ----------------------------------------------------------------------- *)
(* Find basic block list *)
val bbList = Graph.findBB (Graph.nodes g1) g1 [];
(* Filter list for duplicates *)
val bbList = Graph.isolateBB bbList [];
(* The final basic block list *)
val bbList = Graph.newBBCyc g1 (Graph.nodes g1) bbList;

val bbGraph = makeGraph g1 bbList (GlobalGraph.MapGraph.empty) bbList;

val genMap = GlobalGraph.MapGraph.empty;
val killMap = GlobalGraph.MapGraph.empty;

(* ------------------------------------------------------------------------------ *)

val gkResult = Graph.makeKillGenSetOfBasicBlocks g1 useMap defMap [] (deConstructGlobal (GlobalGraph.nodes bbGraph));
val genMap = GlobalGraph.MapGraph.empty;
val killMap = GlobalGraph.MapGraph.empty;
val mapResult = makeDualMap genMap killMap gkResult
val genMap = #1 mapResult;
val killMap = #2 mapResult;

val inMap = GlobalGraph.MapGraph.empty;
val outMap = GlobalGraph.MapGraph.empty;
val result = GlobalGraph.fixedPtCalcInMapOutMap bbGraph inMap outMap genMap killMap;

(* ----------------------------------------------------------------------- *)

val calcMap = #1 result;
GlobalGraph.MapGraph.listItemsi calcMap;
val ISet = GlobalGraph.MapGraph.find(calcMap, GlobalNode.GN (StringNode.N "I1", StringNode.N "I2"));
makeStringsOutOfSetOfAtomSet [] (AtomSet.listItems (getProper (AtomSet.empty) ISet));

(* ------------------------------------------------------------------------- *)
(* ----------------------------------------------------------------------- *)
val inMap = Graph.MapGraph.empty;
val outMap = Graph.MapGraph.empty;
val result = Graph.fixedPtCalcInMapOutMap g1 inMap outMap useMap defMap;
(* ----------------------------------------------------------------------- *)

val calcMap = #1 result;
Graph.MapGraph.listItemsi calcMap;
val ISet = Graph.MapGraph.find(calcMap, (StringNode.N "I1"));
makeStringsOutOfSetOfAtomSet [] (AtomSet.listItems (getProper (AtomSet.empty) ISet));



OS.Process.exit(OS.Process.success);
