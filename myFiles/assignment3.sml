datatype GrmItem = T of string | NT of string;
datatype GrmRule = R of GrmItem * (GrmItem list);
type GrmRules = GrmRule list;

fun compareNT (NT x, NT y) = String.compare(x, y)
  | compareNT (T x, T y) = String.compare(x, y)
  | compareNT _ = LESS;

structure Map = RedBlackMapFn (struct
    type ord_key = GrmItem
    val compare = compareNT
end);


fun getProperList NONE = []
  | getProperList rest = valOf(rest);

fun getProperBool NONE = 0
  | getProperBool rest = valOf(rest);



(* Init *)
fun isNullable nullMap x = getProperBool(Map.find(nullMap, x));

fun initNull nullMap [] = (Map.insert(nullMap, T " ", 1))
  | initNull nullMap ((NT x, [(T " ")])::rules) = initNull (Map.insert(nullMap, NT x, 1)) rules
  | initNull nullMap (rule::rules) = initNull nullMap rules;


fun initPFirst firstMap [] = firstMap
  | initPFirst firstMap ((T item)::rest) = initPFirst (Map.insert(firstMap, (T item), [(T item)])) rest
  | initPFirst firstMap (_::rest) = initPFirst firstMap rest;

fun initRuleFirst firstMap (NT x, ((T item)::_)) = (Map.insert(firstMap, NT x, (T item)::(getProperList(Map.find(firstMap, NT x)))))
  | initRuleFirst firstMap _ = firstMap;

fun initFirst firstMap [] = firstMap
  | initFirst firstMap ((NT x, production)::rules) = initFirst (initRuleFirst (initPFirst firstMap production) (NT x, production)) rules
  | initFirst firstMap (rule::rules) = initFirst firstMap rules;


fun checkNullable nullMap [] = 1
  | checkNullable nullMap (y::items) = if (isNullable nullMap y = 1) then (checkNullable nullMap items) else 0;

fun analizeNullable nullMap (NT x) [] = Map.insert(nullMap, NT x, 1)
  | analizeNullable nullMap (NT x) ys = if ((checkNullable nullMap ys) = 1) then (Map.insert(nullMap, NT x, 1)) else nullMap
  | analizeNullable nullMap _ _ = nullMap;


fun analizeFirst _ firstMap (NT x) [] yi =
    let
      val xFirst = getProperList(Map.find(firstMap, NT x))
      val yiFirst = getProperList(Map.find(firstMap, yi))
    in (Map.insert(firstMap, NT x, xFirst@yiFirst)) end
  | analizeFirst nullMap firstMap (NT x) ys yi = if ((checkNullable nullMap ys) = 1) then
    let
      val xFirst = getProperList(Map.find(firstMap, NT x))
      val yiFirst = getProperList(Map.find(firstMap, yi))
    in (Map.insert(firstMap, NT x, xFirst@yiFirst)) end else firstMap
  | analizeFirst _ firstMap _ _ _ = firstMap;

fun analizeFollow _ followMap (NT x) yi [] =
    let
      val xFollow = getProperList(Map.find(followMap, NT x))
      val yiFollow = getProperList(Map.find(followMap, yi))
    in (Map.insert(followMap, yi, xFollow@yiFollow)) end
  | analizeFollow nullMap followMap (NT x) yi ys = if ((checkNullable nullMap ys) = 1) then
    let
      val xFollow = getProperList(Map.find(followMap, NT x))
      val yiFollow = getProperList(Map.find(followMap, yi))
    in (Map.insert(followMap, yi, xFollow@yiFollow)) end else followMap
  | analizeFollow _ followMap _ _ _ = followMap;

fun analizeBoth nullMap firstMap followMap yi [] yj =
    let
      val yiFollow = getProperList(Map.find(followMap, yi))
      val yjFirst = getProperList(Map.find(firstMap, yj))
    in (Map.insert(followMap, yi, yiFollow@yjFirst)) end
  | analizeBoth nullMap firstMap followMap yi ys yj = if ((checkNullable nullMap ys) = 1) then
    let
      val yiFollow = getProperList(Map.find(followMap, yi))
      val yjFirst = getProperList(Map.find(firstMap, yj))
    in (Map.insert(followMap, yi, yiFollow@yjFirst)) end else followMap;

fun runInner nullMap firstMap followMap yi part1 [] = followMap
  | runInner nullMap firstMap followMap yi part1 part2 =
      runInner nullMap firstMap (analizeBoth nullMap firstMap followMap yi part1 (hd part2)) yi (part1@[(hd part2)]) (tl part2);


fun runOuter nullMap firstMap followMap (NT x) part1 yi [] = (nullMap, (analizeFirst nullMap firstMap (NT x) part1 yi), (analizeFollow nullMap followMap (NT x) yi []))
  | runOuter nullMap firstMap followMap (NT x) part1 yi part2 =
      let
        val FM = (analizeFirst nullMap firstMap (NT x) part1 yi)
        val FLM = (analizeFollow nullMap followMap (NT x) yi part2)
      in (runOuter nullMap FM (runInner nullMap FM FLM yi part1 part2) (NT x) (part1@[yi]) (hd part2) (tl part2)) end
  | runOuter nullMap firstMap followMap _ _ _ _ = (nullMap, firstMap, followMap);

fun ff nullMap firstMap followMap [] = (nullMap, firstMap, followMap)
  | ff nullMap firstMap followMap (((NT x), production)::rules) =
      let
        val (NM, FM, FLM) = (runOuter (analizeNullable nullMap (NT x) production) firstMap followMap (NT x) [] (hd production) (tl production))
      in (ff NM FM FLM rules) end
  | ff nullMap firstMap followMap _ = (nullMap, firstMap, followMap);

fun onlyAnalizeNullable nullMap [] = nullMap
  | onlyAnalizeNullable nullMap (((NT x), production)::rules) = onlyAnalizeNullable (analizeNullable nullMap (NT x) production) rules
  | onlyAnalizeNullable nullMap _ = nullMap;


fun isolate [] = []
  | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs);

fun isolateAll [] = []
  | isolateAll (x::xs) = isolate(x)::isolateAll(xs);

fun compareNull nullMap1 nullMap2 =
  let
    val l1 = Map.listItems(nullMap1)
    val l2 = Map.listItems(nullMap2)
  in if l1 = l2 then true else false end;

fun compareLL f1 f2 =
  let
    val l1 = Map.listItems(f1)
    val l2 = Map.listItems(f2)
  in if (isolateAll l1) = (isolateAll l2) then true else false end;


fun repeatIter nullMap firstMap followMap rules =
  let
    val (NM, FM, FLM) = (ff nullMap firstMap followMap rules);
  in
    if (((compareNull NM nullMap) andalso (compareLL firstMap FM)) andalso (compareLL followMap FLM)) then (NM, FM, FLM)
    else (repeatIter NM FM FLM rules)
  end;

(*
A -> BC
B -> Eps
C -> Eps
*)


val rules = [((NT "Z"), [(T "d")]), ((NT "Z"), [(NT "X"), (NT "Y"), (NT "Z")]), ((NT "X"), [(NT "Y")]), ((NT "Y"), [(T " ")]), ((NT "Y"), [(T "c")]), ((NT "Y"), [(T "a")])];
val eNM = Map.empty;
val eFM = Map.empty;
val rFLM = Map.empty;

val rNM = initNull eNM rules;
val rFM = initFirst eFM rules;

val SOME x = Map.find(rFM, NT "Z");
isolate x;

(*
Map.find(rFM, NT "X");
Map.find(rFM, NT "Y");
Map.find(rFM, NT "Z");

Map.find(rFLM, NT "X");
Map.find(rFLM, NT "Y");
Map.find(rFLM, NT "Z");*)

val (rNM, rFM, rFLM) = (repeatIter rNM rFM rFLM rules);

Map.listItems(rNM);
isolateAll (Map.listItems(rFM));
isolateAll (Map.listItems(rFLM));

val SOME x = Map.find(rFM, NT "Z");
isolate x;


(*
val x = initFirst eFM rules;
Map.listItems(x);*)

fun compareLL ((NT x, T y), (NT a, T b)) = if [x, a] = [y, b] then EQUAL else LESS
  | compareLL _ = LESS;

structure MapLL = RedBlackMapFn (struct
    type ord_key = GrmItem * GrmItem
    val compare = compareLL
end);

fun inList t [] = false
  | inList t (x::xs) = if x = t then true else (inList t xs);

fun forAll (NT x) table [] rule = table
  | forAll (NT x) table (t::ts) rule = forAll (NT x) (MapLL.insert(table, ((NT x), t), rule)) ts rule
  | forAll _ table _ _ = table;


fun LLParsing nullMap firstMap followMap [] table = table
  | LLParsing nullMap firstMap followMap (((NT x), production)::rules) table =
    if isNullable nullMap (NT x) = 1 then forAll (NT x) table (getProperList(Map.find(followMap, NT x))) ((NT x), production)
    else forAll (NT x) table (getProperList(Map.find(firstMap, NT x))) ((NT x), production);




OS.Process.exit(OS.Process.success);
