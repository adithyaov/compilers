(*Helper functions*)
fun len [] = 0
    | len (_::items) = 1 + (len items);

fun isolate [] = []
    | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs)

(*Signature requires for a graph*)
signature NODESIG = sig
    eqtype node
    val compareNodes: (node * node) -> order
end;

(*Basic functor for making the graph with the above signature*)
functor GraphFn (G:NODESIG) = struct
    structure MapGraph = RedBlackMapFn (struct
        type ord_key = G.node
        val compare = G.compareNodes
    end);

    fun getProper ifNone NONE = ifNone
        | getProper ifNone rest = valOf(rest);

    fun containedIn n1 [] = false
        | containedIn n1 (n::ns) = if (G.compareNodes (n1, n) = EQUAL) then true else (containedIn n1 ns);

    fun newNode mapG n = MapGraph.insert(mapG, n, [] : G.node list);
    fun addEdge mapG n ns = MapGraph.insert(mapG, n, (getProper [] (MapGraph.find(mapG, n)))@ns);
    fun nodes mapG = MapGraph.listKeys(mapG);
    fun succ mapG ofN = getProper [] (MapGraph.find(mapG, ofN));
    fun pred [] _ _ = []
        | pred (n::ns) mapG ofN = if containedIn ofN (getProper [] (MapGraph.find(mapG, n))) then n::(pred ns mapG ofN) else (pred ns mapG ofN);
    fun isIn [] n = false
        | isIn (n1::nodes) n = if G.compareNodes (n1, n) = EQUAL then true else isIn nodes n;
end;

(*A string node structure*)
structure StringNode = struct
        datatype node = N of string
        val compareNodes = fn ((N a), (N b)) => String.compare(a, b)
    end;

(*A String graph*)
structure Graph = GraphFn(StringNode);

(*Function for traversing down*)
fun traverseDown G n pN bNode =
    let
        val pred = (Graph.pred (Graph.nodes G) G n)
        val succ = (Graph.succ G n)
    in
        if Graph.isIn [n] bNode then pN else
        if len pred > 1 then pN else
        if len succ > 1 then n else
        if len succ = 0 then n else
            traverseDown G (hd succ) n bNode
    end;

(*Function for traversing up*)
fun traverseUp G n pN bNode =
    let
        val pred = (Graph.pred (Graph.nodes G) G n)
        val succ = (Graph.succ G n)
    in
        if Graph.isIn [n] bNode then n else
        if len succ > 1 then pN else
        if len pred > 1 then n else
        if len pred = 0 then n else
            traverseUp G (hd pred) n bNode
    end;

(*Function for finding the basic blocks*)
fun findBB [] G bb = isolate bb
    | findBB (n::ns) G bb =
        let
            val pred = (Graph.pred (Graph.nodes G) G n)
            val succ = (Graph.succ G n)
        in
            if len pred = 1 andalso len succ = 1 then
              findBB ns G ((traverseUp G (hd pred) n n, traverseDown G (hd succ) n n)::bb)
            else if len succ = 1 then
              findBB ns G ((n, traverseDown G (hd succ) n n)::bb)
            else if len pred = 1 then
              findBB ns G ((traverseUp G (hd pred) n n, n)::bb)
            else findBB ns G ((n, n)::bb)
        end;

fun isInBB [] (a1, b1) = false
    | isInBB ((a, b)::zs) (a1, b1) = if a1 = a then true else isInBB zs (a1, b1);

fun isolateBB [] newList = newList
    | isolateBB ((a, b)::xs) newList = if isInBB newList (a, b) = false then isolateBB xs ((a, b)::newList) else isolateBB xs newList;

fun rmDup [] _ = []
    | rmDup [x] _ = [x]
    | rmDup ((a, b)::bbList) l = if l = a then bbList else (a, b)::(rmDup bbList l);

(* filter for detecting cycles *)
fun newBBCyc G [] bbList = bbList
    | newBBCyc G (n::ns) bbList =
        let
            val pred = (Graph.pred (Graph.nodes G) G n)
            val succ = (Graph.succ G n)
        in
            if (len pred = 1) andalso (len succ = 1) then
                let
                    val (pU::_) = pred
                    val (sU::_) = succ
                    val p = (Graph.pred (Graph.nodes G) G pU)
                    val s = (Graph.succ G sU)
                in
                    if (len p = 1) andalso (len s = 1) then
                        newBBCyc G ns (rmDup bbList n)
                    else newBBCyc G ns bbList
                end
            else newBBCyc G ns bbList
        end;

(*Creating the Global graph structure*)
structure GlobalNode = struct
        datatype node = GN of (StringNode.node * StringNode.node)
        val compareNodes = fn (GN (a1, b1), GN (a2, b2)) =>
            if StringNode.compareNodes(a1, a2) = EQUAL then StringNode.compareNodes(b1, b2) else StringNode.compareNodes(a1, a2)
    end;

(*GlobalGraph == Graph of basic blocks as nodes*)
structure GlobalGraph = GraphFn(GlobalNode);

(*Functions for making the GlobalGraph*)

fun findFlow nodes [] = []
    | findFlow nodes ((n1, n2)::ns) = if Graph.isIn nodes n1 then ((GlobalNode.GN (n1, n2))::(findFlow nodes ns)) else findFlow nodes ns;

fun findSucc G (n1, n2) bbList = findFlow (Graph.succ G n2) bbList;


fun makeGraph G [] newG bbFull = newG
    | makeGraph G (bb::bbList) newG bbFull =
          makeGraph G bbList (GlobalGraph.addEdge (GlobalGraph.newNode newG (GlobalNode.GN bb)) (GlobalNode.GN bb) (findSucc G bb bbFull)) bbFull;




(* Making the graph *)

val x = Graph.MapGraph.empty;
val x = Graph.newNode x (StringNode.N "a");
val x = Graph.newNode x (StringNode.N "b");
val x = Graph.newNode x (StringNode.N "c");
val x = Graph.newNode x (StringNode.N "d");
val x = Graph.newNode x (StringNode.N "e");
val x = Graph.newNode x (StringNode.N "f");
val x = Graph.newNode x (StringNode.N "g");
val x = Graph.newNode x (StringNode.N "h");
val x = Graph.addEdge x (StringNode.N "a") [(StringNode.N "b")];
val x = Graph.addEdge x (StringNode.N "b") [(StringNode.N "c")];
val x = Graph.addEdge x (StringNode.N "c") [(StringNode.N "d")];
val x = Graph.addEdge x (StringNode.N "d") [(StringNode.N "e")];
val x = Graph.addEdge x (StringNode.N "e") [(StringNode.N "f")];
val x = Graph.addEdge x (StringNode.N "f") [(StringNode.N "g")];
val x = Graph.addEdge x (StringNode.N "g") [(StringNode.N "h")];
val x = Graph.addEdge x (StringNode.N "a") [(StringNode.N "e")];


(*
val x = Graph.MapGraph.empty;
val x = Graph.newNode x (StringNode.N "a");
val x = Graph.newNode x (StringNode.N "b");
val x = Graph.newNode x (StringNode.N "c");
val x = Graph.newNode x (StringNode.N "d");
val x = Graph.addEdge x (StringNode.N "a") [(StringNode.N "b")];
val x = Graph.addEdge x (StringNode.N "b") [(StringNode.N "c")];
val x = Graph.addEdge x (StringNode.N "c") [(StringNode.N "a")];
val x = Graph.addEdge x (StringNode.N "d") [(StringNode.N "c")];*)
(*
val x = Graph.MapGraph.empty;
val x = Graph.newNode x (StringNode.N "a");
val x = Graph.newNode x (StringNode.N "b");
val x = Graph.newNode x (StringNode.N "c");
val x = Graph.addEdge x (StringNode.N "a") [(StringNode.N "b")];
val x = Graph.addEdge x (StringNode.N "b") [(StringNode.N "c")];
val x = Graph.addEdge x (StringNode.N "c") [(StringNode.N "a")];*)


(* Find basic block list *)
val bbList = findBB (Graph.nodes x) x [];
(* Filter list for duplicates *)
val bbList = isolateBB bbList [];
(* The final basic block list *)
val bbList = newBBCyc x (Graph.nodes x) bbList;

val bbGraph = makeGraph x bbList (GlobalGraph.MapGraph.empty) bbList;
GlobalGraph.MapGraph.listItemsi bbGraph;

(* --------------------------------------------------------------------------------- *)

structure HelperMap = RedBlackMapFn (struct
    type ord_key = StringNode.node
    val compare = StringNode.compareNodes
end);

structure GlobalHelperMap = RedBlackMapFn (struct
    type ord_key = GlobalNode.node
    val compare = GlobalNode.compareNodes
end);


(* inset, outset, useset, defset are defined over HelperMap *)


fun getProper ifNone NONE = ifNone
    | getProper ifNone rest = valOf(rest);


(* Initialize *)
fun initInMap inMap useMap [] = inMap
    | initInMap inMap useMap (node::nodes) =
        let
          val valueToInsert = getProper (AtomSet.empty) (HelperMap.find(useMap, node))
          val newInMap = HelperMap.insert(inMap, node, valueToInsert)
        in
          initInMap newInMap useMap nodes
        end;

fun GlobalInitInMap inMap useMap [] = inMap
    | GlobalInitInMap inMap useMap (node::nodes) =
        let
          val valueToInsert = getProper (AtomSet.empty) (GlobalHelperMap.find(useMap, node))
          val newInMap = GlobalHelperMap.insert(inMap, node, valueToInsert)
        in
          GlobalInitInMap newInMap useMap nodes
        end;


fun unionAtomSets theMap [] = AtomSet.empty
    | unionAtomSets theMap (node::nodes) =
        AtomSet.union(getProper (AtomSet.empty) (HelperMap.find(theMap, node)), unionAtomSets theMap nodes);

fun findSetStep G inMap outMap useMap defMap [] = (inMap, outMap)
    | findSetStep G inMap outMap useMap defMap (node::nodes) =
        let
          val useSet = getProper (AtomSet.empty) (HelperMap.find(useMap, node))
          val defSet = getProper (AtomSet.empty) (HelperMap.find(defMap, node))
          val newOutSet = unionAtomSets inMap (Graph.succ G node)
          val newInSet = AtomSet.union(useSet, AtomSet.difference(newOutSet, defSet))
          val newInMap = HelperMap.insert(inMap, node, newInSet)
          val newOutMap = HelperMap.insert(outMap, node, newOutSet)
        in
          findSetStep G newInMap newOutMap useMap defMap nodes
        end;

fun compareAtomMaps map1 map2 [] = true
    | compareAtomMaps map1 map2 (node::nodes) =
        let
          val map1Set = getProper (AtomSet.empty) (HelperMap.find(map1, node))
          val map2Set = getProper (AtomSet.empty) (HelperMap.find(map2, node))
        in
          if AtomSet.equal(map1Set, map2Set) then compareAtomMaps map1 map2 nodes else false
        end;

fun fixedPtCalcInMapOutMap G inMap outMap useMap defMap =
      let
        val (newInMap, newOutMap) = findSetStep G inMap outMap useMap defMap (Graph.nodes G)
        val compareInMaps = compareAtomMaps newInMap inMap (Graph.nodes G)
        val compareOutMaps = compareAtomMaps newOutMap outMap (Graph.nodes G)
      in
        if (compareInMaps andalso compareOutMaps) then (inMap, outMap)
        else fixedPtCalcInMapOutMap G newInMap newOutMap useMap defMap
      end;

fun GlobalFixedPtCalcInMapOutMap G inMap outMap useMap defMap =
      let
        val (newInMap, newOutMap) = findSetStep G inMap outMap useMap defMap (GlobalGraph.nodes G)
        val compareInMaps = compareAtomMaps newInMap inMap (GlobalGraph.nodes G)
        val compareOutMaps = compareAtomMaps newOutMap outMap (GlobalGraph.nodes G)
      in
        if (compareInMaps andalso compareOutMaps) then (inMap, outMap)
        else GlobalFixedPtCalcInMapOutMap G newInMap newOutMap useMap defMap
      end;


fun makeAtomSetOutOfSetOfStrings set [] = set
    | makeAtomSetOutOfSetOfStrings set (x::xs) = makeAtomSetOutOfSetOfStrings (AtomSet.add(set, Atom.atom x)) xs;

fun makeStringsOutOfSetOfAtomSet strList [] = strList
    | makeStringsOutOfSetOfAtomSet strList (x::xs) = makeStringsOutOfSetOfAtomSet ((Atom.toString x)::strList) xs;

fun killGenSet useMap defMap genSet killSet [] = (genSet, killSet)
    | killGenSet useMap defMap genSet killSet (node::nodes) =
        let
          val useSet = getProper (AtomSet.empty) (HelperMap.find(useMap, node))
          val defSet = getProper (AtomSet.empty) (HelperMap.find(defMap, node))
          val newGenSet = AtomSet.union(genSet, AtomSet.difference(useSet, killSet))
          val newKillSet = AtomSet.union(killSet, defSet)
        in
          killGenSet useMap defMap newGenSet newKillSet nodes
        end;

fun getInstList G startInst result =
      let
        val succ = Graph.succ G startInst;
      in
        if len succ = 1 then
          if Graph.isIn result startInst then
            result
          else getInstList G (hd succ) (startInst::result)
        else (startInst::result)
      end;

fun makeKillGenSetOfBasicBlocks G useMap defMap result [] = result
    | makeKillGenSetOfBasicBlocks G useMap defMap result ((GlobalNode.GN (startInst, endInst))::nodes) =
        let
          val instList = getInstList G startInst []
          val (genSet, killSet) = killGenSet useMap defMap (AtomSet.empty) (AtomSet.empty) instList
        in
          makeKillGenSetOfBasicBlocks G useMap defMap (((startInst, endInst), (genSet, killSet))::result) nodes
        end;


val g1 = Graph.MapGraph.empty;
val g1 = Graph.newNode g1 (StringNode.N "I1");
val g1 = Graph.newNode g1 (StringNode.N "I2");
val g1 = Graph.newNode g1 (StringNode.N "I3");
val g1 = Graph.newNode g1 (StringNode.N "I4");
val g1 = Graph.addEdge g1 (StringNode.N "I1") [(StringNode.N "I2")];
val g1 = Graph.addEdge g1 (StringNode.N "I2") [(StringNode.N "I3")];
val g1 = Graph.addEdge g1 (StringNode.N "I2") [(StringNode.N "I4")];
val useMap = HelperMap.empty;
val defMap = HelperMap.empty;
val useMap = HelperMap.insert(useMap, StringNode.N "I1", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["y", "z"]);
val defMap = HelperMap.insert(defMap, StringNode.N "I1", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x"]);
val useMap = HelperMap.insert(useMap, StringNode.N "I2", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w", "z"]);
val defMap = HelperMap.insert(defMap, StringNode.N "I2", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x"]);
val useMap = HelperMap.insert(useMap, StringNode.N "I3", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w", "z"]);
val defMap = HelperMap.insert(defMap, StringNode.N "I3", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["w"]);
val useMap = HelperMap.insert(useMap, StringNode.N "I4", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["x", "y"]);
val defMap = HelperMap.insert(defMap, StringNode.N "I4", makeAtomSetOutOfSetOfStrings (AtomSet.empty) ["z"]);


(* ----------------------------------------------------------------------- *)
(* Find basic block list *)
val bbList = findBB (Graph.nodes g1) g1 [];
(* Filter list for duplicates *)
val bbList = isolateBB bbList [];
(* The final basic block list *)
val bbList = newBBCyc g1 (Graph.nodes g1) bbList;

val bbGraph = makeGraph g1 bbList (GlobalGraph.MapGraph.empty) bbList;

val genMap = GlobalHelperMap.empty;
val killMap = GlobalHelperMap.empty;

fun populateGenMapAndKillMap genMap killMap [] = (genMap, killMap)
    | populateGenMapAndKillMap genMap killMap ((entry, (genSet, killSet))::nodes) =
        let
          val mainGenMap = GlobalHelperMap.insert(genMap, entry, genSet)
          val manKillMap = GlobalHelperMap.insert(killMap, entry, killSet)
        in
          populateGenMapAndKillMap mainGenMap manKillMap nodes
        end;

val gkMap = makeKillGenSetOfBasicBlocks g1 useMap defMap [] (GlobalGraph.nodes bbGraph);
val genMap = hd gkMap;
val killMap = hd (tl gkMap);
val inMap = GlobalHelperMap.empty;
val outMap = GlobalHelperMap.empty;
val result = fixedPtCalcInMapOutMap bbGraph inMap outMap genMap killMap;

(* ----------------------------------------------------------------------- *)
(* ----------------------------------------------------------------------- *)
val inMap = HelperMap.empty;
val inMap = initInMap inMap useMap (Graph.nodes g1);
val outMap = HelperMap.empty;
val result = fixedPtCalcInMapOutMap g1 inMap outMap useMap defMap;
(* ----------------------------------------------------------------------- *)

val calcInMap = #1 result;
val calcOutMap = #1 result;
HelperMap.listItemsi calcInMap;
HelperMap.listItemsi calcOutMap;
val I1InSet = HelperMap.find(calcInMap, (StringNode.N "I1"));
makeStringsOutOfSetOfAtomSet [] (AtomSet.listItems (getProper (AtomSet.empty) I1InSet));



OS.Process.exit(OS.Process.success);
