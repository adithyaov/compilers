(*
	use "assignment0.sml";
*)

datatype tree = nullTree | node of tree * int * tree;


fun inOrder nullTree = []
	| inOrder (node(nullTree, currentNode : int, nullTree)) = [currentNode]
	| inOrder (node(lTree, currentNode : int, rTree)) = (inOrder lTree) @ [currentNode] @ (inOrder rTree);
	
fun changeRoot nullTree x = nullTree
	| changeRoot (node(lTree, currentNode, rTree)) x = (node(lTree, x, rTree));

fun getRoot nullTree = ~1
	| getRoot (node(lTree, currentNode, rTree)) = currentNode;

fun rotateAC nullTree = nullTree
	| rotateAC (node(nullTree, currentNode : int, nullTree)) = nullTree
	| rotateAC (node(lTree, currentNode: int, rTree)) = (node(changeRoot lTree currentNode, getRoot(rTree), changeRoot rTree (getRoot(lTree))));

fun rotateNewAC nullTree = nullTree
	| rotateNewAC (node(lTree, currentNode : int, nullTree)) = (node(lTree, currentNode : int, nullTree))
	| rotateNewAC (node(lTree, currentNode: int, (node(rTreeLTree, rNode: int, rTreeRTree)))) = (node((node(lTree , currentNode, rTreeLTree)), rNode, rTreeRTree));

inOrder(node((node(nullTree, 1, nullTree)), 2, (node(nullTree, 3, nullTree))));
inOrder(rotateAC(node((node(nullTree, 1, nullTree)), 2, (node(nullTree, 3, nullTree)))));
