datatype GrmItem = T of string | NT of string;
datatype GrmRule = R of GrmItem * (GrmItem list);
type GrmRules = GrmRule list;

fun compareNT (NT x, NT y) = String.compare(x, y)
  | compareNT (T x, T y) = String.compare(x, y)
  | compareNT _ = LESS;

structure Map = RedBlackMapFn (struct
    type ord_key = GrmItem
    val compare = compareNT
end);


fun getProperList NONE = []
  | getProperList rest = valOf(rest);

fun getProperBool NONE = [0]
  | getProperBool rest = valOf(rest);



(* Init *)
fun reduce [] = 0
  | reduce (x::xs) = if x = 1 then 1 else (reduce xs);

fun isNullable nullMap (NT x) = reduce(getProperBool(Map.find(nullMap, NT x)))
  | isNullable nullMap _ = 0;

fun initNull nullMap [] = (Map.insert(nullMap, T " ", 1::getProperBool(Map.find(nullMap, T " "))))
  | initNull nullMap ((NT x, [(T " ")])::rules) = initNull (Map.insert(nullMap, NT x, 1::getProperBool(Map.find(nullMap, NT x)))) rules
  | initNull nullMap (rule::rules) = initNull nullMap rules;


fun initPFirst firstMap [] = firstMap
  | initPFirst firstMap ((T item)::rest) = initPFirst (Map.insert(firstMap, (T item), [(T item)])) rest
  | initPFirst firstMap (_::rest) = initPFirst firstMap rest;

fun initRuleFirst firstMap (NT x, ((T item)::_)) = (Map.insert(firstMap, NT x, (T item)::(getProperList(Map.find(firstMap, NT x)))))
  | initRuleFirst firstMap _ = firstMap;

fun initFirst firstMap [] = firstMap
  | initFirst firstMap ((NT x, production)::rules) = initFirst (initRuleFirst (initPFirst firstMap production) (NT x, production)) rules
  | initFirst firstMap (rule::rules) = initFirst firstMap rules;


fun checkNullable nullMap [] = 1
  | checkNullable nullMap ((NT y)::items) = if ((isNullable nullMap (NT y)) = 1) then (checkNullable nullMap items) else 0
  | checkNullable nullMap _ = 0;

fun analizeNullable nullMap (NT x) [] = Map.insert(nullMap, NT x, 1::getProperBool(Map.find(nullMap, NT x)))
  | analizeNullable nullMap (NT x) ys = if ((checkNullable nullMap ys) = 1) then (Map.insert(nullMap, NT x, 1::getProperBool(Map.find(nullMap, NT x)))) else nullMap
  | analizeNullable nullMap _ _ = nullMap;

fun analizeFirst _ firstMap (NT x) [] yi =
    let
      val xFirst = getProperList(Map.find(firstMap, NT x))
      val yiFirst = getProperList(Map.find(firstMap, yi))
    in (Map.insert(firstMap, NT x, xFirst@yiFirst)) end
  | analizeFirst nullMap firstMap (NT x) ys yi = if ((checkNullable nullMap ys) = 1) then
    let
      val xFirst = getProperList(Map.find(firstMap, NT x))
      val yiFirst = getProperList(Map.find(firstMap, yi))
    in (Map.insert(firstMap, NT x, xFirst@yiFirst)) end else firstMap
  | analizeFirst _ firstMap _ _ _ = firstMap;

fun analizeFollow _ followMap (NT x) yi [] =
    let
      val xFollow = getProperList(Map.find(followMap, NT x))
      val yiFollow = getProperList(Map.find(followMap, yi))
    in (Map.insert(followMap, yi, xFollow@yiFollow)) end
  | analizeFollow nullMap followMap (NT x) yi ys = if ((checkNullable nullMap ys) = 1) then
    let
      val xFollow = getProperList(Map.find(followMap, NT x))
      val yiFollow = getProperList(Map.find(followMap, yi))
    in (Map.insert(followMap, yi, xFollow@yiFollow)) end else followMap
  | analizeFollow _ followMap _ _ _ = followMap;

fun analizeBoth nullMap firstMap followMap yi [] yj =
    let
      val yiFollow = getProperList(Map.find(followMap, yi))
      val yjFirst = getProperList(Map.find(firstMap, yj))
    in (Map.insert(followMap, yi, yiFollow@yjFirst)) end
  | analizeBoth nullMap firstMap followMap yi ys yj = if ((checkNullable nullMap ys) = 1) then
    let
      val yiFollow = getProperList(Map.find(followMap, yi))
      val yjFirst = getProperList(Map.find(firstMap, yj))
    in (Map.insert(followMap, yi, yiFollow@yjFirst)) end else followMap;

fun runInner nullMap firstMap followMap yi part1 [] = followMap
  | runInner nullMap firstMap followMap yi part1 part2 =
      runInner nullMap firstMap (analizeBoth nullMap firstMap followMap yi part1 (hd part2)) yi (part1@[(hd part2)]) (tl part2);


fun runOuter nullMap firstMap followMap (NT x) part1 yi [] = (nullMap, (analizeFirst nullMap firstMap (NT x) part1 yi), (analizeFollow nullMap followMap (NT x) yi []))
  | runOuter nullMap firstMap followMap (NT x) part1 yi part2 =
      let
        val FM = (analizeFirst nullMap firstMap (NT x) part1 yi)
        val FLM = (analizeFollow nullMap followMap (NT x) yi part2)
      in (runOuter nullMap FM (runInner nullMap FM FLM yi part1 part2) (NT x) (part1@[yi]) (hd part2) (tl part2)) end;

fun ff nullMap firstMap followMap [] = (nullMap, firstMap, followMap)
  | ff nullMap firstMap followMap (((NT x), production)::rules) =
      let
        val (NM, FM, FLM) = (runOuter (analizeNullable nullMap (NT x) production) firstMap followMap (NT x) [] (hd production) (tl production))
      in (ff NM FM FLM rules) end;

fun isolate [] = []
  | isolate (x::xs) = x::isolate(List.filter (fn y => y <> x) xs);


(*
A -> BC
B -> Eps
C -> Eps
*)


val rules = [((NT "Z"), [(T "d")]), ((NT "Z"), [(NT "X"), (NT "Y"), (NT "Z")]), ((NT "X"), [(NT "Y")]), ((NT "Y"), [(T " ")]), ((NT "Y"), [(T "c")]), ((NT "Y"), [(T "a")])];
val eNM = Map.empty;
val eFM = Map.empty;
val rFLM = Map.empty;

val rNM = initNull eNM rules;
val rFM = initFirst eFM rules;

Map.find(rNM, NT "X");
Map.find(rNM, NT "Y");
Map.find(rNM, NT "Z");
(*
Map.find(rFM, NT "X");
Map.find(rFM, NT "Y");
Map.find(rFM, NT "Z");

Map.find(rFLM, NT "X");
Map.find(rFLM, NT "Y");
Map.find(rFLM, NT "Z");*)

val (rNM, rFM, rFLM) = (ff rNM rFM rFLM rules);
(*Map.listItems(rNM);
Map.listItems(rFM);
Map.listItems(rFLM);*)

Map.find(rNM, NT "X");
Map.find(rNM, NT "Y");
Map.find(rNM, NT "Z");
(*
Map.find(rFM, NT "X");
Map.find(rFM, NT "Y");
Map.find(rFM, NT "Z");

Map.find(rFLM, NT "X");
Map.find(rFLM, NT "Y");
Map.find(rFLM, NT "Z");*)


(*val (arNM, arFM, arFLM) = (ff rNM rFM rFLM rules);
Map.listItems(arNM);
Map.listItems(arFM);
Map.listItems(arFLM);*)


(*
val x = initFirst eFM rules;
Map.listItems(x);*)

OS.Process.exit(OS.Process.success);
